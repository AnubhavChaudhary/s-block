## Neural Network
 ![neural](single_layer_nn.jpg)
 <p>In laymann terms a neural network is basically an algorithm that try to mimic the brain,that gets better and better all by itself.<br>  
 A neural network is made up of many hidden layers in which all the nodes of one layer are connected to all the nodes of the next layer and is  used in cases where we have a non linear hypothesis(ex- in speech recognition or facial recognition).<br> 
 The above shown neural network is a sngle hidden layer neural network producing a single output but there can be many of them.</p> 
 
 ## Gradient Descent
 ![gd1](gd1.jpg)
 <p>In machine learning all the predictions are based on a hypothesis,We can measure the accuracy of our hypothesis function by using a cost function .<br>We need to estimate the parameters in the hypothesis function. That's where gradient descent comes in.</p>

![gd2](gd2.jpg)
<p>Gradient Desecent gives us the value of parameters at which the cost function is minimum,which also depends upon the learning rate &alpha;  which determines how much will the alogrithm descend in a single step.


## Neural Network:Working
<p>Our input nodes (layer 1), also known as the "input layer", go into another node (layer 2), which finally outputs the hypothesis function, known as the "output layer".We can have intermediate layers of nodes between the input and output layers called the "hidden layers".<br>In this example, we label these intermediate or "hidden" layer nodes and call them "activation units."<br>
 
  #### Training a Neural Network

  __1.__ Randomly initialize the weights<br>
 __2.__ Implement forward propagation to get cost function for any x<br>
 __3.__ Implement the cost function<br>
__4.__ Implement backpropagation to compute partial derivatives<br>
 __5.__ Use gradient checking to confirm that your backpropagation works. Then disable gradient checking(optional).<br>
  __6.__ Use gradient descent or a built-in optimization function to minimize the cost function with the weights in theta.<br>The values for each of the "activation" nodes is obtained as follows:
</p>

![nn2](nn2.JPG)
<p>After performing forward propagation and getting all the activation values for all the hidden layer in our neural network,the next step is Backpropagation Algorithm to minimize our error.
</p>

### Backpropagtion Algorithm
<p>Backpropagation is simply a method for calculating the partial derivative of the cost function with respect to all of the parameters.<br>The backpropagation algorithm calculates how much the final output values, o1 and o2, are affected by each of the weights.To do this, it calculates partial derivatives, going back from the error function to the neuron that carried a specific weight.</p>

![nn3](nn3.jpg)
<p>Now as we have our partial derivatives we can calculate the optimum value of our weights of each nodes and our neural nwtwork is ready to use.
</p>

![nn4](nn4.jpg)
