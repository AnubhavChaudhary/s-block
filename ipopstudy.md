# Face Recognition
 | Input/Output info  | Amazon Rekognition    | Mirosoft Cognitive Services | 
 | ------------------ | --------------------- | --------------------------- | 
 | __Input :Pythonapi to feed input__ | IndexFaces | C# : DetectWithUrlAsync() or DetectWithStreamAsync()|
 | __Input : Format and specs of input supported__ | __1.__ Amazon Rekognition Image currently supports the JPEG and PNG image formats. You can submit images either as an S3 object or as a byte array. Amazon Rekognition Video operations can analyze videos stored in Amazon S3 buckets. The video must be encoded using the H.264 codec. The supported file formats are MPEG-4 and MOV. __2.__ Amazon Rekognition Image supports image file sizes up to 15MB when passed as an S3 object, and up to 5MB when submitted as an image byte array. Amazon Rekognition Video supports up to 10 GB files and up to 6 hour videos when passed through as an S3 file. | __1.__ The supported input image formats are JPEG, PNG, GIF (the first frame), BMP.__2.__ Image file size should be no larger than 4 MB.  |
 | __Prerequisite__ | As a rule of thumb, please ensure that the smallest object or face present in the image is at least 5% of the size (in pixels) of the shorter image dimension. For example, if you are working with a 1600x900 image, the smallest face or object should be at least 45 pixels in either dimension.| The detectable face size range is 36 x 36 to 4096 x 4096 pixels. Faces outside of this range won't be detected.|  
  | __Features Provided__ | Searchable Image Library,Face-Based User Verification,Sentiment Analysis,Facial Recognition,Image Moderation etc. |Face Detection,Face Identification,Face Verification,Emotion Detection etc.|
  | __Output : Output method__| Describes the face properties such as the bounding box, face ID, image ID of the input image, and external image ID that you assigned  __1.__ Bounding box : Amazon face Rekognition returns bounding boxes coordinates for face/items that are detected in images. __2.__ Confidence :Confidence level that the bounding box contains a face (and not a different object such as a tree). __3.__ ExternalImageId :Identifier that you assign to all the faces in the input image. __4.__ Emotion :The emotions that appear to be expressed on the face, and the confidence level in the determination. __5.__ Image quality :Identifies face image brightness and sharpness. __6.__ Label : Structure containing details about the detected label, including the name, detected instances, parent labels, and level of confidence. __7.__ Gender : Amazon Rekognition makes gender binary (male/female) predictions based on the physical appearance of a face in a particular image.| __1.__ Get the locations and dimensions of faces in an image. __2.__ Get the locations of various face landmarks, such as pupils, nose, and mouth, in an image. __3.__ Get the gender, age, emotion, and other attributes of a detected face.|
  | __Output : Output format__| JSON|JSON|
  | | | |
   # Output sample of Amazon Rekognition
   ``` Json
   "FaceDetails": [ 
      { 
         "AgeRange": { 
            "High": number,
            "Low": number
         },
         "Beard": { 
            "Confidence": number,
            "Value": boolean
         },
         "BoundingBox": { 
            "Height": number,
            "Left": number,
            "Top": number,
            "Width": number
         },
         "Confidence": number,
         "Emotions": [ 
            { 
               "Confidence": number,
               "Type": "string"
            }
         ],
         "Eyeglasses": { 
            "Confidence": number,
            "Value": boolean
         },
         "EyesOpen": { 
            "Confidence": number,
            "Value": boolean
         },
         "Gender": { 
            "Confidence": number,
            "Value": "string"
         },
         "Landmarks": [ 
            { 
               "Type": "string",
               "X": number,
               "Y": number
            }
         ],
         "MouthOpen": { 
            "Confidence": number,
            "Value": boolean
         },
         "Mustache": { 
            "Confidence": number,
            "Value": boolean
         },
         "Pose": { 
            "Pitch": number,
            "Roll": number,
            "Yaw": number
         },
         "Quality": { 
            "Brightness": number,
            "Sharpness": number
         },
         "Smile": { 
            "Confidence": number,
            "Value": boolean
         },
         "Sunglasses": { 
            "Confidence": number,
            "Value": boolean
         }
      }
   ],
   "OrientationCorrection": "string"
}
```
 

 # Output sample Microsoft Cognitive Services
  ```Json
  [
    {
        ""faceId"": ""c5c24a82-6845-4031-9d5d-978df9175426"",
        ""recognitionModel"": ""recognition_02"",
        ""faceRectangle"": {
            ""width"": 78,
            ""height"": 78,
            ""left"": 394,
            ""top"": 54
        },
        ""faceLandmarks"": {
            ""pupilLeft"": {
                ""x"": 412.7,
                ""y"": 78.4
            },
            ""pupilRight"": {
                ""x"": 446.8,
                ""y"": 74.2
            },
            ""noseTip"": {
                ""x"": 437.7,
                ""y"": 92.4
            },
            ""mouthLeft"": {
                ""x"": 417.8,
                ""y"": 114.4
            },
            ""mouthRight"": {
                ""x"": 451.3,
                ""y"": 109.3
            },
            ""eyebrowLeftOuter"": {
                ""x"": 397.9,
                ""y"": 78.5
            },
            ""eyebrowLeftInner"": {
                ""x"": 425.4,
                ""y"": 70.5
            },
            ""eyeLeftOuter"": {
                ""x"": 406.7,
                ""y"": 80.6
            },
            ""eyeLeftTop"": {
                ""x"": 412.2,
                ""y"": 76.2
            },
            ""eyeLeftBottom"": {
                ""x"": 413.0,
                ""y"": 80.1
            },
            ""eyeLeftInner"": {
                ""x"": 418.9,
                ""y"": 78.0
            },
            ""eyebrowRightInner"": {
                ""x"": 4.8,
                ""y"": 69.7
            },
            ""eyebrowRightOuter"": {
                ""x"": 5.5,
                ""y"": 68.5
            },
            ""eyeRightInner"": {
                ""x"": 441.5,
                ""y"": 75.0
            },
            ""eyeRightTop"": {
                ""x"": 446.4,
                ""y"": 71.7
            },
            ""eyeRightBottom"": {
                ""x"": 447.0,
                ""y"": 75.3
            },
            ""eyeRightOuter"": {
                ""x"": 451.7,
                ""y"": 73.4
            },
            ""noseRootLeft"": {
                ""x"": 428.0,
                ""y"": 77.1
            },
            ""noseRootRight"": {
                ""x"": 435.8,
                ""y"": 75.6
            },
            ""noseLeftAlarTop"": {
                ""x"": 428.3,
                ""y"": 89.7
            },
            ""noseRightAlarTop"": {
                ""x"": 442.2,
                ""y"": 87.0
            },
            ""noseLeftAlarOutTip"": {
                ""x"": 424.3,
                ""y"": 96.4
            },
            ""noseRightAlarOutTip"": {
                ""x"": 446.6,
                ""y"": 92.5
            },
            ""upperLipTop"": {
                ""x"": 437.6,
                ""y"": 105.9
            },
            ""upperLipBottom"": {
                ""x"": 437.6,
                ""y"": 108.2
            },
            ""underLipTop"": {
                ""x"": 436.8,
                ""y"": 111.4
            },
            ""underLipBottom"": {
                ""x"": 437.3,
                ""y"": 114.5
            }
        },
        ""faceAttributes"": {
            ""age"": 71.0,
            ""gender"": ""male"",
            ""smile"": 0.88,
            ""facialHair"": {
                ""moustache"": 0.8,
                ""beard"": 0.1,
                ""sideburns"": 0.02
            },
            ""glasses"": ""sunglasses"",
            ""headPose"": {
                ""roll"": 2.1,
                ""yaw"": 3,
                ""pitch"": 1.6
            },
            ""emotion"": {
                ""anger"": 0.575,
                ""contempt"": 0,
                ""disgust"": 0.006,
                ""fear"": 0.008,
                ""happiness"": 0.394,
                ""neutral"": 0.013,
                ""sadness"": 0,
                ""surprise"": 0.004
            },
            ""hair"": {
                ""bald"": 0.0,
                ""invisible"": false,
                ""hairColor"": [
                    {""color"": ""brown"", ""confidence"": 1.0},
                    {""color"": ""blond"", ""confidence"": 0.88},
                    {""color"": ""black"", ""confidence"": 0.48},
                    {""color"": ""other"", ""confidence"": 0.11},
                    {""color"": ""gray"", ""confidence"": 0.07},
                    {""color"": ""red"", ""confidence"": 0.03}
                ]
            },
            ""makeup"": {
                ""eyeMakeup"": true,
                ""lipMakeup"": false
            },
            ""occlusion"": {
                ""foreheadOccluded"": false,
                ""eyeOccluded"": false,
                ""mouthOccluded"": false
            },
            ""accessories"": [
                {""type"": ""headWear"", ""confidence"": 0.99},
                {""type"": ""glasses"", ""confidence"": 1.0},
                {""type"": ""mask"","" confidence"": 0.87}
            ],
            ""blur"": {
                ""blurLevel"": ""Medium"",
                ""value"": 0.51
            },
            ""exposure"": {
                ""exposureLevel"": ""GoodExposure"",
                ""value"": 0.55
            },
            ""noise"": {
                ""noiseLevel"": ""Low"",
                ""value"": 0.12
            }
        }
    }
    ]
    

 

